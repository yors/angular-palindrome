import { async, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PalindromeService } from './palindrome.service';

describe('PalindromeService', () => {
  let service: PalindromeService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PalindromeService]
    });
    service = TestBed.inject(PalindromeService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it(`should create`, async(inject([HttpTestingController, PalindromeService],
    (httpClient: HttpTestingController, apiService: PalindromeService) => {
      expect(apiService).toBeTruthy();
    })));
});
