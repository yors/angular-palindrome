import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PalindromeModel } from '../shared/models/palindrome.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PalindromeService {

  constructor(public http: HttpClient) { }


  /**
   * Get palindrome data
   */
  public getPalindromeDataFromJson(): Observable<PalindromeModel[]> {
    return this.http.get<PalindromeModel[]>('../../assets/flux.json', { responseType: 'json' });
  }
}
