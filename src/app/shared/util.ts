export class Util {

    /**
     * Check if the parameter given is a palindrome
     * @param word the word to check
     * @returns 'OK' if the word is a palindrome else 'KO'
     */
    public static checkPalindromeWord(word: string): string {
        const inverseWord = [...word].reverse().join('');
        return inverseWord === word ? 'OK' : 'KO';
    }

}
