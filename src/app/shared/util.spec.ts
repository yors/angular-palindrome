import { async, TestBed } from '@angular/core/testing';
import { Util } from './util';

describe('Util', () => {

    beforeEach(async(() => {
        return TestBed.configureTestingModule({

        })
            .compileComponents();
    }));

    it('[Palindrome check] Should display KO', () => {

        expect(Util.checkPalindromeWord('palindome')).toBe('KO');
        expect(Util.checkPalindromeWord('stat')).toBe('KO');

    });

    it('[Palindrome check] Should display OK', () => {
        expect(Util.checkPalindromeWord('12345654321')).toBe('OK');
        expect(Util.checkPalindromeWord('stats')).toBe('OK');
    });
});
