import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PalindromeDetailComponent } from './detail/palindrome-detail.component';
import { PalindromeComponent } from './palindrome.component';


export const routes: Routes = [{
  path: 'palindrome/:label',
  component: PalindromeDetailComponent,
},
{
  path: 'palindrome',
  component: PalindromeComponent,
},

{
  path: '',
  redirectTo: 'palindrome'
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PalindromeRoutingModule { }
