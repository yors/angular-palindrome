import { Component, OnInit } from '@angular/core';
import { PalindromeModel } from 'src/app/shared/models/palindrome.model';
import { PalindromeService } from 'src/app/services/palindrome.service';
import { Util } from 'src/app/shared/util';

@Component({
  selector: 'app-palindrome',
  templateUrl: './palindrome.component.html',
  styleUrls: ['./palindrome.component.scss']
})
export class PalindromeComponent implements OnInit {

  public palindromes: PalindromeModel[];

  constructor(public palindromeService: PalindromeService) { }

  ngOnInit(): void {
    this.palindromeService.getPalindromeDataFromJson().subscribe(data => {
      this.palindromes = data;
    });
  }

  /**
   * Check if the parameter given is a palindrome
   * @param id the identifier to check
   * @returns 'true' if the identifier is a palindrome else 'false'
   */
  public isPalindromeId(id): boolean {
    return Util.checkPalindromeWord(id) === 'OK' ? true : false;
  }

}
