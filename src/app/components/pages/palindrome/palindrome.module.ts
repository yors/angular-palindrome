import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PalindromeRoutingModule } from './palindrome-routing.module';
import { PalindromeComponent } from './palindrome.component';
import { PalindromeDetailComponent } from './detail/palindrome-detail.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [PalindromeComponent, PalindromeDetailComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    PalindromeRoutingModule
  ]
})
export class PalindromeModule { }
