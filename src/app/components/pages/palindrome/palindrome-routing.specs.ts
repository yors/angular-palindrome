import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { PalindromeDetailComponent } from "./detail/palindrome-detail.component";
import { routes } from "./palindrome-routing.module";
import { PalindromeComponent } from "./palindrome.component";

describe('Router: App', () => {

    let location: Location;
    let router: Router;
    let fixture;
  
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule.withRoutes(routes)],
        declarations: [
          PalindromeDetailComponent,
          PalindromeComponent,
        ]
      });
  
      router = TestBed.inject(Router);
      location = TestBed.inject(Location);
      router.initialNavigation();

    });

    it('navigate to "" redirects you to /palindrome', fakeAsync(() => {
        router.navigate(['']);
        tick();
        expect(location.pathname).toBe('/palindrome');
      }));

      it('navigate to "" redirects you to /palindrome/toto', fakeAsync(() => {
        router.navigate(['/palindrome/toto']);
        tick();
        expect(location.pathname).toBe('/palindrome/toto');
      }));
  });