import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Util } from 'src/app/shared/util';

@Component({
  selector: 'app-palindrome',
  templateUrl: './palindrome-detail.component.html',
  styleUrls: ['./palindrome-detail.component.scss']
})
export class PalindromeDetailComponent implements OnInit {

  public messagePalindrome: string;

  constructor(private readonly route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.messagePalindrome = Util.checkPalindromeWord(params.get('label'));
    });
  }


}
