import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PalindromeDetailComponent } from './palindrome-detail.component';

describe(' PalindromeDetailComponent', () => {
  let component: PalindromeDetailComponent;
  let fixture: ComponentFixture<PalindromeDetailComponent>;


  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [PalindromeDetailComponent],
      providers: [{
        provide: ActivatedRoute,
        useValue: {
          paramMap: of({
            get: (key: string) => {
              return 'stats';
            }
          })
        }
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalindromeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create PalindromeComponent', () => {
    expect(component).toBeTruthy();
  });
});
